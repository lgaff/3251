CC=gcc
MAKE_DIR=$(PWD)

override CFLAGS+=-Wall -Werror -I ${MAKE_DIR}
BUILD_DIR=objects
BIN_DIR=bin

COMMON_D=$(MAKE_DIR)/common
DISASM_D=${MAKE_DIR}/disassembler

DISASM_DEPS=$(addprefix ${COMMON_D}/, file.o debug.o opcodes.o)

export MAKE_DIR COMMON_D BUILD_DIR BIN_DIR CFLAGS CC

all:
	@mkdir -p $(BIN_DIR)
	@$(MAKE) -C $(COMMON_D)
	@$(MAKE) -C $(DISASM_D)
	$(CC) $(CFLAGS) $(DISASM_DEPS) $(addprefix ${DISASM_D}/, main.o disassemble.o) -o $(BIN_DIR)/3251d



.PHONY:clean
clean:
	rm -rf $(BIN_DIR)
	@$(MAKE) -C $(COMMON_D) clean
	@$(MAKE) -C $(DISASM_D) clean
